#!/usr/bin/env bash

timestamp() {
  date +%Y-%m-%dT%H:%M:%S.%3NZ
}

validateVar(){
  if [ -z "${1}" ]; then echo "Failed to retrieve CSI driver $2" 1>&2; exit 1; fi
}

# Will stop the execution of the backup script if it finds any command execution error
# as all the operations are critical.
set -e

# Get job name uid through the downward API. This value is store in the labels of the pod just created by the job.
# It is required to run parallel pods in the job and be able to do simultaneously backups in parallel of different PVs.
JOB_UID=$(cat /etc/jobinfo/labels | grep 'job-name' | cut -d'=' -f2 |  tr -d '"')


# Contact the OpenStack manila API to retrieve information about each of the manila shares
# We need this to be able to mount PVs for backup
# See https://clouddocs.web.cern.ch/file_shares/programmatic_access.html
MANILA_URL=$(openstack catalog show manilav2 -f json | jq -r '.endpoints[] | select(.interface == "public") | .url')
validateVar "$MANILA_URL" "MANILA_URL"

# OpenStack token issues will expire after 24h, so we can create several tokens per day
OPENSTACK_MANILA_SECRET=$(openstack token issue -f json | jq -r '.id')
validateVar "$OPENSTACK_MANILA_SECRET" "OPENSTACK_MANILA_SECRET"

# Iterates over all the items of the repo queue identified by the job id and the init name.
while true; do
  ITEM=$(redis-cli -h redis LPOP job-${JOB_UID}-${REDIS_QUEUE_INIT_NAME}-queue)
  if [ -z "$ITEM" ]; then
    echo "No more PV to process"
    exit 0
  fi
      # Get information needed for each of the json queue elements of the repo.
      # This is needed to mount the PVs into the pods to do the backup.
      PV_NAME=$(echo "$ITEM" | jq -r '.metadata.name')
      validateVar "$PV_NAME" "PV_NAME"

      NAMESPACE_CSI_DRIVER=$(echo "$ITEM" | jq -r '.spec.csi.nodeStageSecretRef.namespace')
      validateVar "$NAMESPACE_CSI_DRIVER" "NAMESPACE_CSI_DRIVER"

      # We need this information to access the manila API
      MANILA_SHARE_ID=$(echo "$ITEM" | jq -r '.spec.csi.volumeAttributes.shareID')
      validateVar "$MANILA_SHARE_ID" "MANILA_SHARE_ID"

      MANILA_SHARE_ACCESS_ID=$(echo "$ITEM" | jq -r '.spec.csi.volumeAttributes.shareAccessID')
      validateVar "$MANILA_SHARE_ACCESS_ID" "MANILA_SHARE_ACCESS_ID"

      MANILA_EXPORT_LOCATIONS=$(curl -X GET -H "X-Auth-Token: $OPENSTACK_MANILA_SECRET" -H "X-Openstack-Manila-Api-Version: 2.51" $MANILA_URL/shares/$MANILA_SHARE_ID/export_locations)
      validateVar "$MANILA_EXPORT_LOCATIONS" "MANILA_EXPORT_LOCATIONS"

      # Stores monitors and path of the PV, similar to
      # 137.138.121.135:6789,188.184.85.133:6789,188.184.91.157:6789:/volumes/_nogroup/337f5361-bee2-415b-af8e-53eaec1add43
      CEPHFS_PATH_PV=$(echo $MANILA_EXPORT_LOCATIONS | jq -r '.export_locations[]?.path')
      validateVar "$CEPHFS_PATH_PV" "CEPHFS_PATH_PV"

      # Stores the userKey credentials needed to manually mount CephFS PVs
      MANILA_ACCESS_RULES=$(curl -X GET -H "X-Auth-Token: $OPENSTACK_MANILA_SECRET" -H "X-Openstack-Manila-Api-Version: 2.51" $MANILA_URL/share-access-rules/$MANILA_SHARE_ACCESS_ID)
      validateVar "$MANILA_ACCESS_RULES" "MANILA_ACCESS_RULES"

      CEPHFS_USERKEY=$(echo $MANILA_ACCESS_RULES | jq -r '.access.access_key')
      validateVar "$CEPHFS_USERKEY" "CEPHFS_USERKEY"

      # We need to export RESTIC_REPOSITORY to a new path as we now backup each of the PVs
      # separately into a different folder per PV (See https://its.cern.ch/jira/browse/CIPAAS-605)
      export RESTIC_REPOSITORY="${RESTIC_REPO_BASE}/${PV_NAME}"
      # In case there is a new PV to backup, there won't be any restic repo for it yet, so we need to create it with `restic init`
      restic list locks || restic init

      # It makes sure when the backup started and by which pod
      oc annotate pv/"$PV_NAME" backup-cephfs-volumes.cern.ch/backup-started-at="$(timestamp)" --overwrite=true
      oc annotate pv/"$PV_NAME" backup-cephfs-volumes.cern.ch/backup-started-by="$(hostname)" --overwrite=true

      echo mounting "$PV_NAME" in /mnt JOB_UID: "$JOB_UID" ...
      mount -t ceph "$CEPHFS_PATH_PV" -o name="$PV_NAME",noatime,secret="$CEPHFS_USERKEY" /mnt

      echo "backing up PV $PV_NAME JOB_UID: $JOB_UID ..."
      if ! restic backup /mnt --host="$PV_NAME" --tag=cronjob --tag="$PV_NAME"; then
        echo "ERROR backing up pv $PV_NAME by $(hostname)"
        oc annotate pv/"$PV_NAME" backup-cephfs-volumes.cern.ch/backup-failure-at="$(timestamp)" --overwrite=true
        oc annotate pv/"$PV_NAME" backup-cephfs-volumes.cern.ch/backup-failure-by="$(hostname)" --overwrite=true
      else

        echo "$PV_NAME" backed up

        # It annotates the success of the backup into the PV
        echo annotating and labeling PV "$PV_NAME" JOB_UID: "$JOB_UID" ...
        oc annotate pv/"$PV_NAME" backup-cephfs-volumes.cern.ch/backup-success-at="$(timestamp)" --overwrite=true
        oc annotate pv/"$PV_NAME" backup-cephfs-volumes.cern.ch/backup-success-by="$(hostname)" --overwrite=true

        # Push metrics into the prometheus group identified by cephfs_volume_last_backup{job="cephfs_backup_pv", persistentvolume="pv_name", status="backup_succeeded"} date +%s
        cat <<EOF | curl --data-binary @- ${pushgateway_service}/metrics/job/"cephfs_backup_pv"/persistentvolume/"$PV_NAME"/status/"backup_succeeded"
            # TYPE cephfs_volume_last_backup gauge
            # HELP cephfs_volume_last_backup job="cephfs_backup_pv" persistentvolume="pv_name" status="backup_succeeded"
            cephfs_volume_last_backup $(date '+%s.%N' | sed 's/N$//')
EOF
      fi
      # Unmount pv from /mnt earlier mounted
      echo unmounting "$PV_NAME" from /mnt JOB_UID: "$JOB_UID"  ...
      umount /mnt

      # We remove /root/.cache/ in each iteration to prevent restic backups to run out of memory and fail the cronjobs we run,
      # as we detected this malfunction in our infra.
      echo "cleaning up /root/.cache/*"
      rm -rf /root/.cache/*

done
